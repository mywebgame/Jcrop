<%@ page language="java" contentType="text/html; charset=utf-8"
    pageEncoding="utf-8"%>
 <%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>图像上传</title>
<link rel="stylesheet" type="text/css" href="<%=path%>/resource/uploadify/uploadify.css">
<link rel="stylesheet" type="text/css" href="<%=path%>/resource/jquery.Jcrop.css">

<script type="text/javascript" src="<%=path%>/resource/jquery.min.js"></script>
<script type="text/javascript" src="<%=path%>/resource/uploadify/jquery.uploadify.min.js"></script>
<script type="text/javascript" src="<%=path%>/resource/jquery.Jcrop.js"></script>

</head>
<body>
	<form method="post" action="">
		<!-- <input type="file" value="图像上传" id="image" name="image"/>-->
		<input type="file" id="file_upload_1" name="image"/>
		<input type="button" id="but-submit" value="确认"/>		
	</form>
	<div id="cropzoom_container">
        <img alt="用户图像" id="srcImage" src="" name="srcImage" width="400px" height="270px"> 
        <input type="text" id="srcname" name="srcname">
        <input type="text" id="x" name="x" />  
        <input type="text" id="y" name="y" />  
        <input type="text" id="w" name="w" />  
  	    <input type="text" id="h" name="h" />
    </div>
    <div id="cardImg">
                 用户图像<img id="userImg" name="userImg" src="" width="100px" height="60px"/>
    </div>
</body>
<script type="text/javascript">
$(function() {
	$("#file_upload_1").uploadify({
		height        : 30,
		multi : false,
		fileSizeLimit : '2MB',
		fileDataName : 'image',
		progressData : 'percentage',
		'fileTypeDesc' : '格式:jpg',     //描述
		fileTypeExts : '*.jpg',
		buttonText : '选择jpg文件上传',
		swf           : '<%=path%>/resource/uploadify/uploadify.swf',
		uploader      : '<%=path%>/upload!uploadimage.action',
		width         : 140,
		onUploadSuccess : function(file, data, response) {
			var json = jQuery.parseJSON(data);
			var url=json.filePath;
			$("#srcname").val(json.name);
    		$("#srcImage").attr("src",'<%=path%>/upload!download.action?filePath='+url);   		
    		$("#srcImage").Jcrop({   
    		    aspectRatio : 1,  
                onChange : showCoords,  
                onSelect : showCoords,  
                minSize :[100,100]          
            });    		
		}
	});
	 
}); 
 function showCoords(obj) { 
   $("#x").val(obj.x);  
   $("#y").val(obj.y);  
   $("#w").val(obj.w);  
   $("#h").val(obj.h);   
 }
 $("#but-submit").click(function(){
		var xlength=$("#x").val();
		var ylength=$("#y").val();
		var wlength=$("#w").val();
		var hlength=$("#h").val();
		var imagename=$("#srcname").val();
		$.ajax({
			method:"post",
			data:{x:xlength,y:ylength,w:wlength,h:hlength,imagename:imagename},
			url:"<%=path%>/upload!imageCut.action",	
			success:function(data){
				var json = jQuery.parseJSON(data);
				var uri=json.filePath;
	    		$("#userImg").attr("src",'<%=path%>/upload!download.action?filePath='+uri);  
			}
		});
		 
	});
</script>
</html>